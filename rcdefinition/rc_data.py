#!/usr/bin/env python3
# Copyright (c) 2018 - 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""SKT and pipeline rc_data serialization."""
from configparser import RawConfigParser
import sys

_NO_DEFAULT = object()


def parse_config_data(data, require_sections=True):
    """Parse config data (str or utf-8 bytes) into sectioned dict.

    The value under [section][key] is converted to int/float/str/list(str).

    Arguments:
        string: str or utf-8 bytes to read using ConfigParser
        require_sections: if False, wrap input in dummy section

    Returns:
        None or dict with all the values

    """
    if not data:
        return {}

    parser = RawConfigParser()
    parser.optionxform = lambda option: option
    # accept string or utf-8 bytes
    string = data if isinstance(data, str) else data.decode('utf-8')
    string = string if require_sections else ('[dummy]\n' + string)
    parser.read_string(string)

    # make 1st level of keys that match section names
    results = {section: {} for section in parser.sections()}

    for section in parser.sections():
        # make 2nd level of keys that match keys under respective sections
        for key, _ in parser.items(section, raw=True):
            # convert inner value, but don't convert 1/0 to True/False
            try:
                values = parser.getint(section, key, raw=True)
            except ValueError:
                results[section][key] = parser.get(section, key, raw=True)
            else:
                results[section][key] = values

    return results if require_sections else results['dummy']


class DefinitionBase:
    """Base class to simplify dataclass serialization (config, yaml, dict)."""

    def update(self, new_data):
        """Update the internal dict with new data."""
        for key, value in new_data.items():
            setattr(self, key, value)

    def items(self):
        # pylint: disable=no-member
        """Return dict-like data."""
        results = [(key, getattr(self, key, None)) for key in
                   self.__annotations__]
        return results

    def to_mapping(self):
        """Convert to kci-db compatible mapping."""
        mapping = {}
        for key, value in self.items():
            try:
                mapping[key] = value.to_mapping()
            except AttributeError:
                if value is not None:
                    mapping[key] = value

        return mapping

    def dicts_to_classes(self, dict_data):
        # pylint: disable=no-member
        """Convert attributes that contain dict data into classes according to their annotations."""
        for key, actual_type in self.__annotations__.items():
            # create empty values for non-required attributes
            val = getattr(self, key, None)
            try:
                if key not in dict_data and val is not _NO_DEFAULT:
                    if actual_type in (int, float, bool, str, list) or 'enum' in str(actual_type):
                        dict_data[key] = val
                    else:
                        dict_data[key] = {}
            except TypeError:
                continue

        for key, value in filter(lambda kv: kv[0] in self.__annotations__, dict_data.items()):
            actual_type = self.__annotations__[key]
            if 'List[' in str(actual_type):
                cltype = actual_type.__args__[0]
                nested_values = [cltype(item) for item in value]
                setattr(self, key, nested_values)
                continue
            if actual_type is dict:
                setattr(self, key, value)
                continue

            # handle NoneTypes like x: int = None
            if value is None:
                setattr(self, key, value)
                continue

            try:
                setattr(self, key, actual_type(value))
            except TypeError:
                sys.stderr.write(f'conversion failed for {key} {value}'
                                 f' {actual_type}\n')
                raise

    def __init__(self, dict_data=None):
        """Create an object."""
        self.safe_init({} if dict_data is None else dict_data)
        self.dicts_to_classes({} if dict_data is None else dict_data)
        self.check_for_missing_args()

    def safe_init(self, data_dict):
        # pylint: disable=no-member
        """Create an object including dynamic attributes.

        The dynamic attributes should be removed in the future.
        """
        dynamic_kwargs = {}
        for key, value in data_dict.items():
            if key not in self.__annotations__:
                dynamic_kwargs[key] = value
                # create annotations for undefined keys
                self.__annotations__[key] = type(value)

            setattr(self, key, value)

        alert_keys = [key for key in dynamic_kwargs if
                      'patch_data_' not in key]
        if alert_keys:
            missing_keys = ", ".join(alert_keys)
            cls_name = self.__class__.__name__
            msg = f'code-issue: following keys are not part of the' \
                  f' datastructure ({cls_name}) definition: {missing_keys}\n'
            sys.stderr.write(msg)

    def check_for_missing_args(self):
        # pylint: disable=no-member
        """Raise exception if required attribute isn't set.

        We have to use this way of making attributes required. Otherwise
        positional attributes would prevent us from using inheritance with
        dataclass classes in any meaningful way.

        Args:
            obj: an instance of a class decorated with dataclass,
                 to check for attributes
        Raises: TypeError when required attribute is missing
        """
        missing_args = []
        for key in self.__annotations__:
            if getattr(self, key, None) is _NO_DEFAULT:
                missing_args.append(key)

        if missing_args:
            missing = ', '.join(missing_args)
            raise TypeError(f"__init__ missing {len(missing_args)} required "
                            f"arguments: {missing}")


class StateData(DefinitionBase):
    # pylint: disable=invalid-name, too-many-instance-attributes
    """SKT rc [state] section extended with everything else in [state]."""

    debug_kernel: str = None
    # Status of targeted tests:
    #  -1 - no targeted tests are expected
    #   0 - targeted tests are expected but missing
    #   1 - targeted tests are available
    targeted_tests: int = None
    targeted_tests_list: str = None
    kernel_version: str = None

    def __init__(self, data_dict):
        """Create the object."""
        super().__init__(data_dict)


class SKTData(DefinitionBase):
    """Datastructure for all SKT data."""

    state: StateData

    def __getattribute__(self, item):
        """Get object attribute."""
        return object.__getattribute__(self, item)

    @classmethod
    def deserialize(cls, str_data):
        # pylint: disable=no-member
        """Deserialize string into this object."""
        return SKTData(parse_config_data(str_data))

    def serialize(self):
        """Serialize the object into string."""
        return self.serialize2config()

    def serialize2config(self):
        # pylint: disable=no-member
        """Serialize object into a string with [section]."""
        # This can be read with ConfigParser.
        data = ''
        for section in self.__annotations__:
            data += f'[{section}]\n'
            section_data = getattr(self, section, None)
            if section_data:
                for key in section_data.__annotations__:
                    value = getattr(section_data, key, None)
                    if value is not None:
                        data += f'{key} = {value}\n'
            data += '\n'

        return data
